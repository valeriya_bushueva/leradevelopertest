﻿using UnityEngine;

namespace Shooter
{
    public class BulletConfiguration : MonoBehaviour
    {
        [SerializeField] private float speed = 1;
        [SerializeField] private float force = 1;

        public void Reinitialize(float speed, float force)
        {
            this.speed = speed;
            this.force = force;
        }

        public float Speed => speed;
        public float Force => force;
    }
}
