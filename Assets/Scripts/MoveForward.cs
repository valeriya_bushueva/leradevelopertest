using UnityEngine;

namespace Shooter
{
     public class MoveForward : MonoBehaviour
    {
        [SerializeField] private BulletConfiguration bulletProperties;

        private void Update()
        {
            transform.Translate(Vector3.forward * Time.deltaTime * bulletProperties.Speed);
        }
    }
}

