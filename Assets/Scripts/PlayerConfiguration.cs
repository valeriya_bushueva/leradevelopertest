﻿using UnityEngine;

namespace Shooter
{
    public class PlayerConfiguration : MonoBehaviour
    {
        [SerializeField] private PlayerStatPreset playerStatPreset;
        public float PlayerSpeed => playerStatPreset.playerSpeed;
        public float FireRate => playerStatPreset.fireRate;
        public GameObject BulletPrefab => playerStatPreset.bulletPrefab;

        public float BulletSpeed => playerStatPreset.bulletSpeed;
        public float BulletForce => playerStatPreset.bulletForce;
    }
}