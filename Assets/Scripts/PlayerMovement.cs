﻿using UnityEngine;
using UnityEngine.AI;

namespace Shooter
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private NavMeshAgent agent;
        [SerializeField] private float checkApproveRange = 0.5f;
        [SerializeField] private PlayerConfiguration playerConfiguration;
        [SerializeField] private PlayerAnimationHandler playerAnimationHandler;
        private float clickCheckTimer;
        private float CurrentSpeed => agent.velocity.magnitude;

        private void Start()
        {
            agent.speed = playerConfiguration.PlayerSpeed;
        }

        void Update()
        {
            RefreshAnimationProperties();
            ClickMovement();
        }

        private void ClickMovement()
        {
            if (Input.GetMouseButtonDown(0))
            {
                clickCheckTimer = 0;
            }

            if (Input.GetMouseButton(0))
            {
                clickCheckTimer += Time.deltaTime;
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (clickCheckTimer <= checkApproveRange)
                {
                    RaycastHit hit;
                    if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
                    {
                        agent.destination = hit.point;
                    }
                }
            }
        }

        private void RefreshAnimationProperties()
        {
            if (playerAnimationHandler)
            {
                playerAnimationHandler.SetAnimationSpeed(CurrentSpeed);
            }
        }
    }
}

