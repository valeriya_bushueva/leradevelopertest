﻿using UnityEngine;

namespace Shooter
{
    public class PlayerAnimationHandler : MonoBehaviour
    {
        [SerializeField] private Rigidbody rigidBody;
        [SerializeField] private Animator animator;
        [SerializeField] private Transform aimTargetTransform;
        
        private float handAimInterpolation;
        
        private void Update()
        {
            AnimateAim();
        }

        public void SetHandAimInterpolation(float weight)
        {
            handAimInterpolation = weight;
        }

        public void SetAnimationSpeed(float speed)
        {
            animator.SetFloat("Speed", speed);
        }
        
        private void AnimateAim()
        {
            rigidBody.MoveRotation(Quaternion.LookRotation(aimTargetTransform.position - transform.position));
        } 

        public void OnAnimatorIK()
        {
            // Hand
            animator.SetIKPositionWeight(AvatarIKGoal.RightHand,handAimInterpolation);
            animator.SetIKPosition(AvatarIKGoal.RightHand, aimTargetTransform.position);
        
            // Head
            animator.SetLookAtWeight(handAimInterpolation);
            animator.SetLookAtPosition(aimTargetTransform.position);
        }
    }
}