﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Shooter
{
     public class EnemyController : MonoBehaviour
    {
        [SerializeField] private Transform enemyAimBodyTarget;
        [SerializeField] private RagdollController ragdollswitch;

        private static List<Transform> allEnemies = new List<Transform>();

        public static List<Transform> AllEnemies => allEnemies;

        private void OnEnable()
        {
            allEnemies.Add(enemyAimBodyTarget);
        }

        private void OnDisable()
        {
            allEnemies.Remove(enemyAimBodyTarget);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Bullet"))
            {
                ragdollswitch.Ragdoll();

                BulletConfiguration bulletConfiguration = other.GetComponent<BulletConfiguration>();

                ragdollswitch.ApplyExplosion(bulletConfiguration.Force);

                allEnemies.Remove(enemyAimBodyTarget);
            }
        }
    }
}
