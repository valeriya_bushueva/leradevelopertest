﻿
using UnityEngine;

public class RagdollController : MonoBehaviour
{
    [SerializeField] private Animator animator;
    private Rigidbody[] rigidbodies;
    private const float explosionRadius = 100;
    
    void Start()
    {
        rigidbodies = GetComponentsInChildren<Rigidbody>();
        SetKinematic(true);
    }

    [ContextMenu("Ragdoll")]
    public void Ragdoll()
    {
        animator.enabled = false;
        SetKinematic(false);
    }

    public void ApplyExplosion(float force)
    {
        foreach (var rb in rigidbodies)
        {
            rb.AddExplosionForce(force, transform.position, explosionRadius);
        }
    }

    public void SetKinematic(bool state)
    {
        foreach (Rigidbody rb in rigidbodies)
        {
            rb.isKinematic = state;
        }
    }
}
