﻿using System.Collections;
using UnityEngine;

namespace Shooter
{
    public class PlayerController : MonoBehaviour
    {
        private enum ShootState
        {
            ControlShoot,
            AutoShoot
        }

        [SerializeField] private Transform throwPositionFrom;
        [SerializeField] private GameObject gun;
        [SerializeField] private string shootSpotTag;
        [SerializeField] private PlayerConfiguration playerConfiguration;
        [SerializeField] private PlayerAnimationHandler playerAnimationHandler;
        [SerializeField] private float lookRadius = 5f;
        [SerializeField] private Transform enemyTarget;
        [SerializeField] private GameObject firePartical;
        [SerializeField] private Transform aimTargetTransform;
        [SerializeField] private LayerMask mouseAimMask;
        [SerializeField] private ShootState shootState;
        [SerializeField] private bool isReadyToShoot;
        
        private Camera mainCamera;
        private Coroutine currentShootCoroutine;

        public float FireRate => playerConfiguration.FireRate;

        private void Start()
        {
            mainCamera = Camera.main;
            isReadyToShoot = false;
        }

        private void Update()
        {
            if (isReadyToShoot)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    shootState = ShootState.ControlShoot;
                    TryStartShoot();
                }

                if (Input.GetMouseButtonUp(0))
                {
                    shootState = ShootState.AutoShoot;
                }


                if (shootState == ShootState.ControlShoot)
                {
                    ControlAim();
                }
                else if (shootState == ShootState.AutoShoot)
                {
                    FindAndAimEnemy();
                }

            }
        }

        private void FindAndAimEnemy()
        {
            Transform nearestEnemy = null;
            float nearestDistance = float.MaxValue;

            foreach (var enemy in EnemyController.AllEnemies)
            {
                float distance = Vector3.Distance(transform.position, enemy.position);

                if (distance > lookRadius)
                    continue;

                if (distance < nearestDistance)
                {
                    nearestEnemy = enemy;
                    nearestDistance = distance;
                }
            }

            if (nearestEnemy != null)
            {
                enemyTarget = nearestEnemy.transform;
                TryStartShoot();
            }
            else
            {
                enemyTarget = null;
                TryStopShoot();
            }

            EnemyAim();
        }

        private void ControlAim()
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, mouseAimMask))
            {
                aimTargetTransform.position = hit.point;
            }
        }

        private void EnemyAim()
        {
            if (enemyTarget != null)
            {
                aimTargetTransform.position = enemyTarget.position;
            }
        }


        private void TryStartShoot()
        {
            if (currentShootCoroutine != null)
                return;

            currentShootCoroutine = StartCoroutine(ShootBullet());
        }

        private void TryStopShoot()
        {
            if (currentShootCoroutine == null)
                return;

            StopCoroutine(currentShootCoroutine);
            currentShootCoroutine = null;
        }

        
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(shootSpotTag))
            {
                playerAnimationHandler.SetHandAimInterpolation(1);
                gun.SetActive(true);
                isReadyToShoot = true;
                shootState = ShootState.AutoShoot;
            }

        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(shootSpotTag))
            {
                playerAnimationHandler.SetHandAimInterpolation(0);
                gun.SetActive(false);
                isReadyToShoot = false;
                TryStopShoot();
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, lookRadius);
        }


        private IEnumerator ShootBullet()
        {
            while (true)
            {
                GameObject newBullet = Instantiate(playerConfiguration.BulletPrefab, throwPositionFrom.position,
                    throwPositionFrom.transform.rotation);

                newBullet.GetComponent<BulletConfiguration>()
                    .Reinitialize(playerConfiguration.BulletSpeed, playerConfiguration.BulletForce);

                Instantiate(firePartical, throwPositionFrom.position, firePartical.transform.rotation);
                yield return new WaitForSeconds(playerConfiguration.FireRate);
            }
        }
    }
}
