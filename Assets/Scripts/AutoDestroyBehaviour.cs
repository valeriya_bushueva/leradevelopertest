﻿using System.Collections;
using UnityEngine;

namespace Shooter
{
    public class AutoDestroyBehaviour : MonoBehaviour
    {
        [SerializeField] private float destroyAfter = 2;

        void Start()
        {
            StartCoroutine(WaitDestroy());
        }

        private IEnumerator WaitDestroy()
        {
            yield return new WaitForSeconds(destroyAfter);
            Destroy(gameObject);
        }
    }
}
