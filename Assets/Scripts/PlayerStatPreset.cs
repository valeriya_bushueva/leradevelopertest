﻿using UnityEngine;

[CreateAssetMenu(fileName = "newPlayerStatPreset", menuName = "PlayerStatPreset")]
public class PlayerStatPreset : ScriptableObject
{
  public float bulletForce;
  public float bulletSpeed;
  public float playerSpeed;
  public float fireRate;
  public GameObject bulletPrefab;
}
